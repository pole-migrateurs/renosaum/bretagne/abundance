#############################
#          CPUE_1           #
#############################
# CPUE_1 is an abundance index computed at the regional scale by pooling all the logbooks collected in Brittany each year.
# It represents the number of fish caught per hour of fishing during the first fishing period (fishing period during which only MSW individuals can be caught)
NA NA NA NA NA NA NA NA 0.01346 0.00589 0.00325 NA 0.00603 0.00471 0.00402 0.00413 0.00655 0.00909 0.01521 0.01955 0.0123 0.01676 0.01213 0.00279 0.00758 0.01727 0.01125 0.00596 0.00888 0.0033 0.00402
# END