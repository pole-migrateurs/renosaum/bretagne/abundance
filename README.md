#  Taking full advantage of the diverse assemblage of data at hand to produce time series of abundance: a case study on Atlantic salmon populations of Brittany
 
This gitlab page has been created with the aim of giving readers access to most of the materials used in the article:  
*Taking full advantage of the diverse assemblage of data at hand to produce time series of abundance: a case study on Atlantic  salmon populations of Brittany*  
This article has been written by Lebot Clément <sup>1</sup>, Arago Marie-André <sup>2</sup>, Beaulaton Laurent <sup>3</sup>, Germis Gaëlle <sup>4</sup>, Nevoux Marie <sup>5</sup>, Rivot Etienne <sup>5</sup> and Prévost Etienne <sup>1</sup>; and published in the [Canadian Journal of Fisheries and Aquatic Sciences 79(4), 533-547](https://dx.doi.org/10.1139/cjfas-2020-0368).

<sup>1</sup> Université de Pau et des Pays de l’Adour, e2s UPPA, INRAE, OFB, Institut Agro, ECOBIOP, Management of Diadromous Fish in their Environment , Saint-Pée-sur-Nivelle, France.  
<sup>2</sup> Direction Bretagne, OFB, Rennes, France.  
<sup>3</sup> Service Conservation et Gestion Durable des Espèces Exploitées, Direction de la Recherche et de l'Appui Scientifique, OFB, Rennes, France; Management of Diadromous Fish in their Environment, OFB, INRAE, Institut Agro, Université de Pau et des Pays de l’Adour, Rennes, France.  
<sup>4</sup> Bretagne Grands Migrateurs, Rennes, France.  
<sup>5</sup> DECOD (Ecosystem Dynamics and Sustainability), Institut Agro, IFREMER, INRAE, Rennes, France; Management of Diadromous Fish in their Environment, OFB, INRA, Institut Agro, Université de Pau et des Pays de l’Adour, Rennes, France.  

## Abstract

Estimation of abundance with wide spatiotemporal coverage is essential to the assessment and management of wild populations. But, in many cases, data available to estimate abundance time series have diverse forms, variable quality over space and time and they stem from multiple data collection procedures. We developed a hierarchical Bayesian modelling (HBM) approach that take full advantage of the diverse assemblage of data at hand to estimate homogeneous time series of abundances irrespective of the data collection procedure. We apply our approach to the estimation of adult abundances of 18 Atlantic salmon (*Salmo salar*) populations of Brittany (France) from 1987 to 2017 using catch statistics, environmental covariates, and fishing effort. Additional data of total or partial abundance collected in four closely monitored populations are also integrated into the analysis. The HBM framework allows the transfer of information from the closely monitored populations to the others. Our results reveal no clear trend in the abundance of adult returns in Brittany over the period studied.
